package ex00;

import oms3.annotations.*;

public class Component {

    @In public String message;

    @Execute
    public void run() {
        System.out.println(message);
    }
}