package org.modelome.oms.hello;

import oms3.annotations.Execute;
import oms3.annotations.In;


public class Component {

  @In
  public String message;

  @Execute
  public void run() {
    System.out.println(message);
  }

}
