# OMS "Hello World" Example

This project is the simplest example of how to develop a simulation model that is compatible with
the Java OMS (Object Modeling System) standard v3.2.

Because this is a tutorial, the Eclipse configuration files are included in the repository to
minimise the effort needed to get started in Eclipse.

## Notes

Still trying to figure out how to set up OMS projects the normal Java way. This is built from the
template project, and not quite working yet. OMS seems to expect everything to go through the
console GUI, or for the developer to know what they're doing. It's complicated.